let room__item = document.querySelector(".room__item"); 
if (room__item) {
//  
/*
$('[data-fancybox="gallery1"]').fancybox({  
      loop: true,
      keyboard: true,  
      zoomOpacity: true,
      imageScale : false, 
});
 */
//  owl courusel
$(document).ready(function(){
    $('.owl-carousel').owlCarousel({  
        nav:true,
        dots: false,  
        margin: 10,
        navText : [`
        <svg width="30" height="48" viewBox="0 0 30 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M24.9497 46.6332L27.6834 43.8995L7.7849 24L27.6834 4.10055L24.9497 1.36686L2.31656 24L24.9497 46.6332Z" fill="#115AED"/>
            <path d="M24.9507 0L0.948761 24L24.9497 48L29.0503 43.8995L9.15176 24L29.0512 4.10053L24.9507 0ZM3.68342 24L24.9507 2.73368L26.3175 4.10053L6.41807 24L26.3166 43.8995L24.9497 45.2663L3.68342 24Z" fill="#115AED"/>
        </svg> 
        `, `
        <svg width="30" height="48" viewBox="0 0 30 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.05027 1.36682L2.31657 4.10051L22.2151 24L2.31657 43.8995L5.05027 46.6331L27.6834 24L5.05027 1.36682Z" fill="#115AED"/>
            <path d="M5.0493 48L29.0512 24L5.05027 0L0.949735 4.10053L20.8482 24L0.94877 43.8995L5.0493 48ZM26.3166 24L5.0493 45.2663L3.68246 43.8995L23.5819 24L3.68342 4.10053L5.05027 2.73369L26.3166 24Z" fill="#115AED"/>
        </svg> 
        `],
        responsive:{
            0:{
                items:1
            },
            478:{
                items:1 
            },
            479:{
                items:2
            },
            768:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
  });
/// 

}
//
let room_block = document.querySelector(".room_block")

if (room_block) { 
    $(document).ready(function(){
        $('.owl-carousel-detail').owlCarousel({  
            nav:true,
            dots: true,  
            center: true,
            loop:false,
            margin: 15,
            navText : [`
            <svg width="30" height="48" viewBox="0 0 30 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M24.9497 46.6332L27.6834 43.8995L7.7849 24L27.6834 4.10055L24.9497 1.36686L2.31656 24L24.9497 46.6332Z" fill="#115AED"/>
                <path d="M24.9507 0L0.948761 24L24.9497 48L29.0503 43.8995L9.15176 24L29.0512 4.10053L24.9507 0ZM3.68342 24L24.9507 2.73368L26.3175 4.10053L6.41807 24L26.3166 43.8995L24.9497 45.2663L3.68342 24Z" fill="#115AED"/>
            </svg> 
            `, `
            <svg width="30" height="48" viewBox="0 0 30 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.05027 1.36682L2.31657 4.10051L22.2151 24L2.31657 43.8995L5.05027 46.6331L27.6834 24L5.05027 1.36682Z" fill="#115AED"/>
                <path d="M5.0493 48L29.0512 24L5.05027 0L0.949735 4.10053L20.8482 24L0.94877 43.8995L5.0493 48ZM26.3166 24L5.0493 45.2663L3.68246 43.8995L23.5819 24L3.68342 4.10053L5.05027 2.73369L26.3166 24Z" fill="#115AED"/>
            </svg> 
            `],
            responsive:{
                0:{
                    items:1 
                },
                478:{
                    items:1 
                },
                479:{
                    items:1 
                },
                768:{
                    items:1 
                },
                1000:{
                    items:1 
                }
            }
        })
      });
}