let filter = document.querySelector(".filters")
if (filter) {
//checkbox list click
let btn_new_value = filter.querySelectorAll(".filter__new-value"); 
for (const btn of btn_new_value) {
    btn.addEventListener("click", function() {
        let btn_list = btn.previousElementSibling; 
        btn_list.classList.toggle("active");
    })
}
//checkbox list oversize
let filter_list = filter.querySelectorAll(".filter__list"); 
for (const list of filter_list) {
  if (list.children.length < 6) {
    list.nextElementSibling.style.cssText = "display: none;"
  }
}

//slider-range
let initials = filter.querySelectorAll(".filter__input");

for (const elem of initials) {
  if (elem.getAttribute("data-priceminset")) { 
      var initial_min_dot = elem.getAttribute("data-priceminset"); 
      var input_min = elem;
  };
  if (elem.getAttribute("data-pricemaxset")) {
    var initial_max_dot = elem.getAttribute("data-pricemaxset");
    var input_max = elem;
  };
}
 
let intial_range = filter.querySelector("#slider-range");

let min_range = Number( intial_range.getAttribute("data-priceRangeMin") );
let max_range = Number( intial_range.getAttribute("data-priceRangeMax") ); 

$( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: min_range,
      max: max_range,
      values: [ initial_min_dot, initial_max_dot ],
      slide: function( event, ui ) {
        $( "#price_min" ).val("от " + ui.values[ 0 ]);
        $( "#price_max" ).val("до " + ui.values[ 1 ]);
        $( "#price_min" )[0].dataset.pricemin =  ui.values[ 0 ];
        $( "#price_max" )[0].dataset.pricemax =  ui.values[ 1 ]; 
      },
      create: function( event, ui ) {
        $( "#price_min" ).val("от " + $( "#slider-range" ).slider( "option", "values")[0]);
        $( "#price_max" ).val("до " + $( "#slider-range" ).slider( "option", "values")[1]);
      },
    });
    standart_value()
  } );

  $("#slider-range").draggable; 
  $(".filter__form")[0].addEventListener("reset", standart_value);
  $( "#price_min" )[0].addEventListener("change", change_slider_dot);
  $( "#price_max" )[0].addEventListener("change", change_slider_dot);
 
  function standart_value() { 
    setTimeout(function() { 
      $( "#slider-range" ).slider( "option", "values", [initial_min_dot, initial_max_dot]);

      $( "#price_min" ).val("от " + $( "#slider-range" ).slider( "option", "values")[0]);
      $( "#price_max" ).val("до " + $( "#slider-range" ).slider( "option", "values")[1]); 
      $( "#price_min" )[0].dataset.pricemin =  $( "#slider-range" ).slider( "option", "values")[0];
      $( "#price_max" )[0].dataset.pricemax =  $( "#slider-range" ).slider( "option", "values")[1]; 
     }, 10);
  }
  
  function change_slider_dot() {
    let actual_min_dot = Number( $( "#price_min" )[0].value.replace(/[^\d;]/g, '') );
    let actual_max_dot = Number( $( "#price_max" )[0].value.replace(/[^\d;]/g, '') );
 
    $( "#slider-range" ).slider( "option", "values", [actual_min_dot, actual_max_dot]);

    $( "#price_min" )[0].dataset.pricemin =  $( "#slider-range" ).slider( "option", "values")[0];
    $( "#price_max" )[0].dataset.pricemax =  $( "#slider-range" ).slider( "option", "values")[1]; 
  }
  
//form-prepocessing
let form = filter.querySelector(".filter__form"); 

form.onsubmit = function()  {
  input_min.value = input_min.value.replace(/[^\d;]/g, '');
  input_max.value = input_max.value.replace(/[^\d;]/g, '');
}
}

