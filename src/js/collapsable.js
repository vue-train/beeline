let informationBlocks = document.querySelectorAll(".information-block");

informationBlocks?.forEach((item) => {
    item?.addEventListener('click', toggleCollapsable);
    item?.addEventListener('mouseleave', closeCollapsable);
});

function toggleCollapsable() {
    if (this.classList.contains('active')) {
        this.classList.remove('active');
    } else {
        this.classList.add('active');
    }
}

function closeCollapsable() {
    this.classList.remove('active');
}
