/*
  "use strict";
//  import $ from 'jquery';
// import jQuery from 'jquery';
  import '../../node_modules/jquery/dist/jquery';

  window.$ = jQuery;

  if ($) {
      // require('bootstrap');

      //JS files
      require ("./base");
      require ("./catalog");
      //require ("./libs/fancybox/jquery.fancybox");
      //style
      require ("../scss/main.scss");
      require ("@fancyapps/fancybox");
}
*/
// Almira syles
import "./libs/jquery.fancybox";
// import "./libs/owl.carousel";
import "./libs/jquery-ui-1.9.2.custom.min";
// import "./libs/jquery-ui";
import "./libs/jquery.ui.touch-punch";
import "./libs/chunk-vendors.39bff7bb";
import "./libs/chunk-vendors.3e6d09ef";
import "./libs/app.0f651af5";
import "./libs/response";
// import "./_map";

//JS files
// import "./base";
import "./catalog";
import "./accordion";
import "./collapsable";
// import "./video";
// import "./room_switcher";
// import "./_rails-effect";
// import "./_room-item";
import "./_modal";
// import "./modal_star";
import "./line_back_switcher";
import "./line_hover_switcher"


// Stylesheet
import "../scss/main.scss";
