const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const FileManagerPlugin = require('filemanager-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  mode: "development",
  context: path.resolve(__dirname, "src"),
  entry: {
    main: "./js/index.js",
    // img: './img/',
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
  },

  resolve: {
    extensions: [".js", ".json", ".png", "svg", "gif", "jpeg", "jpg", "cur", "pptx"],
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@models": path.resolve(__dirname, "src/models"),
    },
  },
  devServer: {
    port: 4205,
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: "./index.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "about-hotel.html",
      template: "./about-hotel.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "questions.html",
      template: "./questions.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "helpfull.html",
      template: "./helpfull.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "login.html",
      template: "./login.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "appeals.html",
      template: "./appeals.html",
      minify: false,
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new HTMLWebpackPlugin({
      filename: "feedback.html",
      template: "./feedback.html",
      minify: false,
    }),

    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].bundle.css",
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: "files", to: "files" },
      ],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: "babel-loader",
            options: {
              presets: ['@babel/preset-env']
            }
        }
    },

      {
        test: /\.html$/,
        include: path.resolve(__dirname, "src/template"),
        loader: "html-loader",
        options: {
          // Disables attributes processing
          minimize: false,
        }
      },

      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader","postcss-loader"],
      },
      {
        test: /\.less$/,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader",  'postcss-loader' ,  "less-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { sourceMap: true, },},
          'postcss-loader' ,
          { loader: "sass-loader", options: { sourceMap: true, },},
      ],
      },
      {
        test: /\.(png|svg|gif|jpg|jpeg|cur|pptx)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
      {
        test: /\.(ttf|woff|woff2|eot|otf)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
    ],
  },
};
